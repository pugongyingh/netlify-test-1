//本地存储{{
function writeStorage(key, value) {
    try {
        if (window.localStorage) {
            localStorage.setItem(key, value);
        } else {
            setCookie(key, value);
        }
    } catch (e) { }
}
function getStorage(key) {
    try {
        var strStoreData = window.localStorage ? localStorage.getItem(key) : getCookie(key);
        return strStoreData;
    } catch (e) { return ''; }
}
function getCookie(cookiename) {
    var result;
    var mycookie = document.cookie;
    var start2 = mycookie.indexOf(cookiename + "=");
    if (start2 > -1) {
        start = mycookie.indexOf("=", start2) + 1;
        var end = mycookie.indexOf(";", start);
        if (end == -1) {
            end = mycookie.length;
        }
        result = unescape(mycookie.substring(start, end));
    }
    return result;
}
function setCookie(cookiename, cookievalue) {
    document.cookie = cookiename + "=" + cookievalue;
}

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

function setpFooter(){
	var P=$('#btn_top');
	$(document).scroll(function (){
		if($(this).scrollTop()>10) P.show();
		else P.hide();
	});
	P.click(function(){
		if (navigator.userAgent.indexOf('Firefox') >= 0) document.documentElement.scrollTop=0;
		else $('body').animate({scrollTop:0}, 'fast');
	});
}